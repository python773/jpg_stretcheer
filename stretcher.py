# -*- coding: utf-8 -*-
"""
Created on Sat Jul 18 13:52:31 2020

@author: m.szczepkowski
"""

from tkinter import filedialog
from tkinter import *
import os
import datetime
import sys
from tkinter import ttk
from collections import Counter
import shutil
from tkinter.ttk import Combobox
import sys
import cv2

spaces = {
    'hsv': (cv2.COLOR_RGB2HSV, cv2.COLOR_HSV2RGB, 2),
    'lab': (cv2.COLOR_RGB2LAB, cv2.COLOR_LAB2RGB, 1),
    'xyz': (cv2.COLOR_RGB2XYZ, cv2.COLOR_XYZ2RGB, 1)
}

def process_clahe_layer(clahe, layer):
    return clahe.apply(layer)

def process(clahe, img, space, layer_idxs):
    from_rgb, to_rgb, _ = spaces[space]
    cvt_img = cv2.cvtColor(img, from_rgb)
    # GDAL layer order
    for layer_idx in layer_idxs:        
        layer = cvt_img[:,:,layer_idx]
        layer = process_clahe_layer(clahe, layer)
        cvt_img[:,:,layer_idx] = layer
    return cv2.cvtColor(cvt_img, to_rgb)

def execute(img, clip_limit, grid_size, space, layer_indexes):
    clahe = cv2.createCLAHE(clipLimit=clip_limit, tileGridSize=(grid_size, grid_size))
    return process(clahe, img, space, layer_indexes)


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def correct(text, integer):
    if "," in text:
        text=text.replace(",",".")
    if integer:
        if "." in text:
            text=text[:text.rfind(".")]
    return text

def splitter(text):
    dictionary={", ":',',". ":",",".":",",";":','," ":',',"_":',',"'":","}
    for i in dictionary:
        if i in text:
            text=text.replace(i,dictionary[i])
    return text

def idx(indexes):
    for i,index in enumerate(indexes):  
        if index<0:
            indexes[i]=0
        elif index>2:
            indexes[i]=2
    return list(set(indexes))

class Listing:
    def __init__(self,master,st=0):
        self.master=master
        master.title('Stretch all jpg in folder')
        #menu
        self.label_input = Label(master, text="Input folders: ")
        self.label_input.grid(row=0, sticky="E")
        self.entry_input=Entry(master, width=60)
        self.entry_input.insert(END,'')
        self.entry_input.grid(row=0,column=1, sticky="E",pady=2)

        #Labels
        self.label_CL = Label(master, text="Clip_limit: ")
        self.label_CL.grid(row=1, sticky="E")
        self.entry_CL=Entry(master, width=5)
        self.entry_CL.insert(END,'1')
        self.entry_CL.grid(row=1,column=1, sticky="W", pady=2)

        self.label_GS = Label(master, text="Grid Size: ")
        self.label_GS.grid(row=2, sticky="E")
        self.entry_GS=Entry(master, width=5)
        self.entry_GS.insert(END,'8')
        self.entry_GS.grid(row=2,column=1, sticky="W", pady=2)
       
        self.label_LI = Label(master, text="Layer_idx: ")
        self.label_LI.grid(row=3, sticky="E")
        self.entry_LI=Entry(master, width=5)
        self.entry_LI.insert(END,'2')
        self.entry_LI.grid(row=3,column=1, sticky="W", pady=2)

        self.label_SP = Label(master, text="Space: ")
        self.label_SP.grid(row=4, sticky="E")
        self.entry_SP=Combobox(master,values=['hsv','lab','xyz'],width=5)
        self.entry_SP.current(0)
        self.entry_SP.grid(row=4,column=1,sticky="W", pady=2)
        self.entry_SP.bind("<<ComboboxSelected>>")
       
        self.button_path = Button(master, text="Get path", command=self.direction, height=1,width=10)
        self.button_path.grid(row=0,column=2, sticky="E",pady=2, padx=2)

        self.button_start = Button(master, text="Start", command=self.main, height=1,width=10)
        self.button_start.grid(row=7,column=2)
        
        self.button_close = Button(master, text="Exit", command=lambda root=root:self.Quit(root), height=1,width=10)
        self.button_close.grid(row=7,column=0,pady=2, padx=2)

        self.label_msg = Label(master, text="", fg="red",font="Verdana 10 bold")
        self.label_msg.grid(row=6, column=1)         

        self.v=IntVar()
        self.Radiobutton=Radiobutton(master, text="Overwrite images", variable=self.v, value=1).grid(row=5, column=1, sticky="W", padx=60)
        self.Radiobutton=Radiobutton(master, text="Create new images", variable=self.v, value=2).grid(row=5, column=1, sticky="E")
        self.v.set(1)
        
    def Quit(self,root):
            root.quit()
            root.destroy()
            sys.exit()
        
    def get_dirs(self,path):  #get image list
        self.path=path
        file_list=[]
        for root, dirs, files in os.walk(self.path):
            for name in files:
               filename, file_extension = os.path.splitext(name)
               if file_extension in ['.jpg'] and "stretched" not in filename:
                   file_list.append(os.path.join(root, name))
            
        return file_list

    def MSG(self, message):
        self.message=message
        self.master.update()
        self.label_msg['text']=message
                             
    def direction(self):
        self.MSG("")
        window=Tk()
        window.withdraw()
        window.title("JPGs Folder")
        source_path =  filedialog.askdirectory(parent=window,initialdir=os.getcwd(),title='Please select a directory')
        source_path=source_path.replace("/","\\")
        entry=self.entry_input.get()
               
        if not source_path:
            if not source_path and not entry:
                self.MSG("Select path!")
            return
        else:
            self.entry_input.delete(0,END)
            self.entry_input.insert(0,source_path)
            self.MSG('')


    def main(self):
        clip_limit=float(correct(self.entry_CL.get(),False))                #get clip_limit
        grid_size=int(correct(self.entry_GS.get(),True))                 #get grid_size
        idxs=splitter(self.entry_LI.get()) 
        layer_idxs=idx([int(i) for i in idxs.split(",")]) #get layer_idx
        space=self.entry_SP.get()                     #get space
        
        if not clip_limit or not grid_size or not layer_idxs:
            self.MSG("Please insert parameters values")
        else:
            self.MSG('')
            files=self.get_dirs(self.entry_input.get()) #get jpg to convert
            if not files:
                self.MSG("No *.jpg images found in input folder")
            else:
                for i in range(0,len(files)):
                    self.MSG("Processing "+str(i+1)+"/"+str(len(files))+" files    "+str(round(((i+1)/len(files))*100,1))+"%")
                    try:
                        if self.v.get()==1: #move or copy data
                            out_path=files[i]
                        else:
                            out_path=files[i].replace(".jpg",f"_stretched_{clip_limit}_{grid_size}_{idxs}_{space}.jpg")
                        img = cv2.imread(files[i])
                        out_img = execute(img, clip_limit, grid_size, space, layer_idxs)
                        cv2.imwrite(out_path, out_img)
                    except:
                       self.MSG(f"Problem with {files[i]} image, skipped")
                
                self.MSG("Process ends")


root=Tk()
root.iconbitmap(resource_path('stretcher.ico')) 
bar=Listing(root)
root.mainloop()





